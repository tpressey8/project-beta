from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
# Create your views here.
from common.json import ModelEncoder, DateEncoder
from .models import Service, AutomobileVO, Technician


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
        "vip",
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "name",
        "employee_number",
    ]


class ServiceEncoder(ModelEncoder):
    model = Service
    properties = [
        "id",
        "vin",
        "vip",
        "customer_name",
        "date",
        "time",
        "reason",
        "finished",
        "cancelled",
        "technician",
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }



@require_http_methods(["GET", "POST"])
def api_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )
    else:
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)

        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )

# ---------------------- pk ==== employee_id --------------------------
@require_http_methods(["DELETE", "GET", "PUT"])
def api_technician(request, employee_number):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(employee_number=employee_number)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "this technician does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(employee_number=employee_number)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "this technician does not exist"})
    else: # PUT method
        try:
            content = json.loads(request.body)

            technician = Technician.objects.get(employee_number=employee_number)
            props = ["name", "employee_number"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

# ---------------------- pk ==== vin --------------------------


@require_http_methods(["GET", "POST"])
def api_services(request):
    if request.method == "GET":
        services = Service.objects.all()
        print(services, "************services********")
        return JsonResponse(
            {"services": services},
            encoder=ServiceEncoder
        )
    else:

        content = json.loads(request.body)
        # Get the bin object and put it in the content dict
        try:
            tech = content["technician"]
            technician = Technician.objects.get(name=tech)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician name"},
                status=400,
            )


        services = Service.objects.create(**content)
        return JsonResponse(
            services,
            encoder=ServiceEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_service(request, vin):
    if request.method == "GET":
        try:
            service = Service.objects.filter(vin=vin)
            return JsonResponse(
                {"services": service},
                encoder=ServiceEncoder,
                safe=False
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "invalid VIN number"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            service = Service.objects.get(vin=vin)
            service.delete()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False
            )
        except Service.DoesNotExist:
            return JsonResponse(
                {"message": "this VIN does not exist in service records"}
            )
    else: #PUT
        try:
            content = json.loads(request.body)
            service = Service.objects.get(vin=vin)
            props = ["cancelled", "finished"]
            for prop in props:
                if prop in content:
                    setattr(service, prop, content[prop])
            service.save()
            return JsonResponse(
                service,
                encoder=ServiceEncoder,
                safe=False,
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "Service does not exist"})
            response.status_code = 404
            return response

#**************************
@require_http_methods(["GET","POST"])
def api_automobile_vos(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder
        )
    else:
        content = json.loads(request.body)

        automobile = AutomobileVO.objects.create(**content)

        return JsonResponse(
            automobile,
            encoder=AutomobileVOEncoder,
            safe=False,
        )


@require_http_methods([ "GET",])
def api_automobile_vo(request, vin):
    if request.method == "GET":
        try:
            automobiles = AutomobileVO.objects.filter(automobile__vin__contains=vin)
            return JsonResponse(
                automobiles,
                encoder=ServiceEncoder,
                safe=False
            )
        except Service.DoesNotExist:
            response = JsonResponse({"message": "invalid VIN number"})
            response.status_code = 404
            return response



@require_http_methods(["PUT"])
def api_service_cancelled(request, pk):
    service = Service.objects.get(id=pk)
    service.cancelled = True
    service.save()
    return JsonResponse(
        service,
        encoder=ServiceEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def api_service_finished(request, pk):
    service = Service.objects.get(id=pk)
    service.finished = True
    service.save()
    return JsonResponse(
        service,
        encoder=ServiceEncoder,
        safe=False,
    )
