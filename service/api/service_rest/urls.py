from django.urls import path

from .views import (
    api_service,
    api_services,
    api_technician,
    api_technicians,
    api_automobile_vos,
    api_automobile_vo,
    api_service_finished,
    api_service_cancelled
)

urlpatterns = [
    path(
    "service/",
    api_services,
    name="api_services"
    ),
    path(
    "service/<str:vin>/",
    api_service,
    name="api_service"
    ),
    path(
    "technicians/",
    api_technicians,
    name="api_technicians",
    ),
    path(
    "technicians/<int:employee_number>/",
    api_technician,
    name="api_technician",
    ),
    path(
    "auto_vos/",
    api_automobile_vos,
    name="api_automobile_vos"
    ),
    path(
    "auto_vos/<str:vin>/",
    api_automobile_vo,
    name="api_automobile_vo",
    ),
    path(
    "service_finished/<int:pk>/",
    api_service_finished,
    name="api_service_finished"
    ),
    path(
    "service_cancelled/<int:pk>/",
    api_service_cancelled,
    name="api_service_cancelled"
    ),
]
