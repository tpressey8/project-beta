from django.db import models




class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name



class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)
    vip = models.BooleanField(default=False)
    def __str__(self):
        return self.vin

#*********************************************************
# Create your models here.
class Service(models.Model):

    customer_name = models.CharField(max_length=100)
    date = models.DateTimeField(null=True)
    time = models.CharField(max_length=100)
    reason = models.CharField(max_length=200)
    finished = models.BooleanField(default=False)
    cancelled = models.BooleanField(default=False)
    vin = models.CharField(max_length=17)
    vip = models.BooleanField(default=False)

    technician = models.ForeignKey(
        Technician,
        related_name="service",
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.vin
