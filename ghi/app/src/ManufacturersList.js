import { useEffect, useState } from 'react';

function ManufacturersList() {

    const [manufacturers, setManufacturers] = useState([])

    const fetchData = async () => {
        const manufacturersUrl = await fetch ("http://localhost:8100/api/manufacturers/");
        if (manufacturersUrl.ok){
            const data = await manufacturersUrl.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                </tr>
            </thead>
            <tbody>
                {manufacturers.map(manufacturer =>
                    {
                        return (
                            <tr key={ manufacturer.id }>
                                <td>{ manufacturer.name }</td>
                            </tr>
                        )
                    })}
            </tbody>
        </table>


    )
}

export default ManufacturersList;
