import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturersList from './ManufacturersList';
import ManufacturerForm from './ManufacturerForm';
import ModelList from './ModelList';
import ModelForm from './ModelForm';
import NewAutomobile from './NewAutomobile';
import ListAutomobile from './ListAutomobile';
import SalesPersonForm from './SalesPersonForm';
import SalesCustomerForm from './SalesCustomerForm';
import SaleForm from './SaleForm';
import SalesList from './SalesList';
import NewTechnician from './NewTechnician';
import ListService from './ListService';
import NewService from './NewService';
import ListVinService from './ListVinService';




function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/automobiles/" element={<ListAutomobile />} />
          <Route path="/automobile/new/" element={<NewAutomobile />} />
          <Route path="/manufacturers/" element={<ManufacturersList />} />
          <Route path="/manufacturer/new" element={<ManufacturerForm />} />
          <Route path="/models/" element={<ModelList/>} />
          <Route path="/model/new" element={<ModelForm/>} />
          <Route path="/salesperson/new" element={<SalesPersonForm/>} />
          <Route path="/customers/new" element={<SalesCustomerForm/>} />
          <Route path="/sales/new" element={<SaleForm/>} />
          <Route path="/sales/" element={<SalesList/>} />
          <Route path="/technicians/new" element={<NewTechnician />} />
          <Route path="/services/" element={<ListService />} />
          <Route path="/service/new/" element={<NewService />} />
          <Route path="/servicevin/" element={<ListVinService />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
