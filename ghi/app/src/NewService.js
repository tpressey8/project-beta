import React, {useEffect, useState } from 'react';

function NewService(){

    //set defautl states
    const [customerName, setCustomerName] = useState('');
    const [date, setDate ] = useState('');
    const [time, setTime ] = useState('');
    const [reason, setReason] = useState('');
    // const [finished, setFinished] = useState('');
    // const [cancelled, setCancelled ] = useState('');
    const [vin, setVin] = useState('');
    const [technicians, setTechnicians ] = useState([]);
    const [techName, setTechName] = useState('');
    //fetch data for technicians
    const fetchTechnicianData = async () => {
        const url = 'http://localhost:8080/api/technicians';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    //useEffect on technician data

    useEffect(() => {
        fetchTechnicianData();
    }, []);

    //handle state changes defined

    //------handle submit!--------------
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        // set all the states to match model
        data.customer_name = customerName;
        data.date = date;
        data.time = time;
        data.reason = reason;
        // data.finished = finished;
        // data.cancelled = cancelled;
        data.vin = vin;
        data.technician = techName;

        const serviceUrl = 'http://localhost:8080/api/service/';
        const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const serviceResponse = await fetch(serviceUrl, fetchOptions);
        if (serviceResponse.ok) {
            //setStates back to empty/clear out form ('')
            // const newService = await serviceResponse.json();
            // console.log(newService)

            setCustomerName('')
            setDate('')
            setTime('')
            setReason('')
            // setFinished('')
            // setCancelled('')
            setVin('')
            setTechName('')
        }
    }

    const handleCustomerNameChange = (event)=>{
        setCustomerName (event.target.value)
    }
    const handleDateChange = (event)=>{
        setDate(event.target.value)
    }
    const handleTimeChange = (event)=>{
        setTime(event.target.value)
    }
    const handleReasonChange = (event)=>{
        setReason(event.target.value)
    }
    // const handleFinishedChange = (event)=>{
    //     setFinished(event.target.value)
    // }
    // const handleCancelledChange = (event)=>{
    //     setCancelled(event.target.value)
    // }
    const handleTechNameChange = (event)=>{
        setTechName(event.target.value)
    }


    const handleVinChange = (event)=>{
        setVin(event.target.value)
    }


    //return jsx form
    return (
        <div className="my-5 container">
            <div className='card-body'>
                <form onSubmit={handleSubmit} id="create-service-form">

                    <h1 className='card-title'>Create a service request</h1>
                    <p className='mb-3'>
                        Fill out the request details
                    </p>

                    <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleCustomerNameChange} value={customerName} placeholder="Customer's Name" required type="text" id="customerName" name="customerName" className="form-control" />
                            <label htmlFor="customerName">Customer's Name</label>
                        </div>
                    </div>

                    <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleDateChange} value={date} placeholder="date" required type="date" id="date" name="date" className="form-control" />
                            <label htmlFor="date">Date of service appointment</label>
                        </div>
                    </div>

                    <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleTimeChange} value={time} placeholder="time of appointment" required type="time" id="time" name="time" className="form-control" />
                            <label htmlFor="time">Time of appointment</label>
                        </div>
                    </div>

                    <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleReasonChange} value={reason} placeholder="reason for service" required type="text" id="reason" name="reason" className="form-control" />
                            <label htmlFor="reason">reason for service</label>
                        </div>
                    </div>

                    {/* <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleFinishedChange} value={finished} placeholder="Finished" required type="boolean" id="finished" name="finished" className="form-control" />
                            <label htmlFor="finished">Finished</label>
                        </div>
                    </div>

                    <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleCancelledChange} value={cancelled} placeholder="cancelled" required type="boolean" id="cancelled" name="" className="form-control" />
                            <label htmlFor="cancelled">Cancelled</label>
                        </div>
                    </div> */}

                    <div >
                        <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="automobile vin" required type="text" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">automobile vin</label>
                        </div>
                    </div>

                    <div className="mb-3">
                        <select onChange={handleTechNameChange}  value={techName} name="techName" id="techName" className='form-select' required>
                            <option value="">Choose a technician</option>
                            {technicians.map(tech => {
                            return (
                                <option key={tech.id} value={tech.name}>{tech.name}</option>
                            )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-lg btn-primary">Add the service request!</button>
                </form>
            </div>
        </div>
    );

}


export default NewService;
