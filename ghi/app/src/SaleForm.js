import { useState, useEffect } from 'react';

function SaleForm () {
    const [automobiles, setAutomobiles] = useState([]);
    const [salesPeople, setSalesPeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobile, setAutomobile] = useState('');
    const [salesPerson, setSalesPerson] = useState('');
    const [customer, setCustomer] = useState('');
    const [salePrice, setSalePrice] = useState('');

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value)
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setSalesPerson(value)
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value)
    }

    const handleSalePriceChange = (event) => {
        const value = event.target.value;
        setSalePrice(value)
    }

    const fetchDataAutomobiles = async () => {
        const automobileUrl = 'http://localhost:8090/api/automobiles/';
        const response = await fetch(automobileUrl);

        if(response.ok){
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchDataAutomobiles();
    }, []);


    const fetchDataSalesPeople = async () => {
        const salesPeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salesPeopleUrl);

        if(response.ok){
            const data = await response.json();
            setSalesPeople(data.sales_people);
        }
    }

    useEffect(() => {
        fetchDataSalesPeople();
    }, []);


    const fetchDataCustomers = async () => {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl);

        if(response.ok){
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        fetchDataCustomers();
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.sales_person = salesPerson;
        data.customer = customer;
        data.automobile = automobile;
        if(salePrice[0] !== "$"){
            data.sale_price = "$" + salePrice
        }
        else{
            data.sale_price = salePrice
        }

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {
            // Sends put request to update the sale status of sold auto
            const soldStatusUrl = `http://localhost:8090/api/automobiles/sold/${automobile}`
            const fetchSalesConfig = {
                method: "put",
            }
            const response = await fetch(soldStatusUrl, fetchSalesConfig)

            // Updates automobile drop down to exclude recently sold car
            fetchDataAutomobiles()

            setAutomobile('');
            setSalesPerson('');
            setCustomer('');
            setSalePrice('');

        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">

              <div className="mb-3">
                <select value={automobile} onChange={handleAutomobileChange} required id="automobile" name="automobile" className="form-select">
                  <option value="">Choose an automobile</option>
                  {automobiles.filter((automobile) => automobile.sale_status === false).map(automobile => {
                            return (
                                <option value={automobile.id} key={automobile.id}>
                                    {automobile.vin}
                                </option>
                            );
                        })};
                </select>
              </div>

              <div className="mb-3">
                <select value={salesPerson} onChange={handleSalesPersonChange} required id="sales_person" name="sales_person" className="form-select">
                  <option value="">Choose a sales person</option>
                  {salesPeople.map(salesPerson => {
                            return (
                                <option value={salesPerson.id} key={salesPerson.id}>
                                    {salesPerson.name}
                                </option>
                            );
                        })};
                </select>
              </div>

              <div className="mb-3">
                <select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => {
                            return (
                                <option value={customer.id} key={customer.id}>
                                    {customer.name}
                                </option>
                            );
                        })};
                </select>
              </div>

              <div className="form-floating mb-3">
                <input value={salePrice} onChange={handleSalePriceChange} placeholder="Sale Price" required type="text" name="sale_price" id="sale_price" className="form-control" />
                <label htmlFor="sale_price">Sale price</label>
              </div>

              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default SaleForm;
