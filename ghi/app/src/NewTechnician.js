import React, {useEffect, useState } from 'react';


function NewTechnician(){
    //set some default states
    const [name, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState('');

    //handle state changes
    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.name=name;
        data.employee_number= employeeNumber;

        const technicianUrl = 'http://localhost:8080/api/technicians/';
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            const technicianResponse = await fetch(technicianUrl, fetchOptions);
            if (technicianResponse.ok) {
                setName('')
                setEmployeeNumber('')
            }

    }

    const handleNameChange = (event) =>{
        setName(event.target.value)
    }

    const handleEmployeeNumberChange = (event) =>{
        setEmployeeNumber(event.target.value)
    }

    /// jsx

    return (
            <div className="my-5 container">
                <div className="card-body">
                    <form onSubmit={handleSubmit} id="create-technician-form">
                    <p className="mb-3">
                        Tell us about the technician you wish to add
                    </p>

                    <div >
                        <div >
                            <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Primary car color" required type="text" id="name" name="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                            </div>
                        </div>

                        <div >
                            <div className="form-floating mb-3">
                            <input onChange={handleEmployeeNumberChange} value={employeeNumber} placeholder="employeeNumber" required type="text" id="employeeNumber" name="employeeNumber" className="form-control" />
                            <label htmlFor="employeeNumber">Employee Number</label>
                            </div>
                        </div>
                    </div>
                    <button className="btn btn-lg btn-primary">Add employee to list of technicians!</button>
                    </form>
                </div>
            </div>
    )


}


export default NewTechnician;
