import { useState, useEffect } from 'react';

function ModelList() {

    const [models, setModels] = useState([])

    const fetchData = async () => {
        const modelsUrl = await fetch ("http://localhost:8100/api/models/");
        if (modelsUrl.ok){
            const data = await modelsUrl.json();
            setModels(data.models)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Manufacturer</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {models.map(model =>
                    {
                        return (
                            <tr key={ model.id }>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td>
                                    <img src={model.picture_url} className="img-responsive"/>
                                </td>
                            </tr>
                        )
                    })}
            </tbody>
        </table>
    )
}

export default ModelList;
