import React, {useEffect, useState } from 'react';


function NewAutomobile(){
    const [models, setModels] = useState([]);
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model, setModel] = useState('');

    const fetchData = async () => {
            const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);
            if (response.ok) {
            const data = await response.json();
            setModels(data.models);
            }
        }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        // set all the states to match model
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;
        // send post with formatted data
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchOptions = {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
        };
        const automobileResponse = await fetch(automobileUrl, fetchOptions);
        if (automobileResponse.ok) {
            //setStates back to empty/clear out form ('')
            setColor('')
            setYear('')
            setVin('')
            setModel('')
        }
    }
    // handle state changes
    const handleColorChange = (event) =>{
        setColor(event.target.value)
    }
    const handleYearChange = (event)=>{
        setYear(event.target.value)
    }
    const handleVinChange = (event)=>{
        setVin(event.target.value)
    }
    const handleModelChange = (event)=>{
        setModel(event.target.value)
    }

    // return jsx form
    return (
        <div className="my-5 container">
            <div className="card-body">
                <form onSubmit={handleSubmit} id="create-automobile-form">

                    <h1 className="card-title"></h1>
                    <p className="mb-3">
                    Please select a model for you automobile
                    </p>

                    <div className="mb-3">
                        <select onChange={handleModelChange}  value={model} name="model" id="model" className='form-select' required>
                            <option value="">Choose a model</option>
                            {models.map(model => {
                            return (
                                <option key={model.id} value={model.id}>{model.name}</option>
                            )
                            })}
                        </select>
                    </div>

                    <p className="mb-3">
                        Tell us more about the automobile you want to add to the inventory
                    </p>

                    <div >

                        <div >
                            <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Primary car color" required type="text" id="color" name="color" className="form-control" />
                            <label htmlFor="color">Color</label>
                            </div>
                        </div>

                        <div >
                            <div className="form-floating mb-3">
                            <input onChange={handleYearChange} value={year} placeholder="Year" required type="text" id="year" name="year" className="form-control" />
                            <label htmlFor="name">Year of model</label>
                            </div>
                        </div>

                        <div >
                            <div className="form-floating mb-3">
                            <input onChange={handleVinChange} value={vin} placeholder="vin number" required type="vin" id="vin" name="vin" className="form-control" />
                            <label htmlFor="vin">vin Number</label>
                            </div>
                        </div>

                    </div>

                    <button className="btn btn-lg btn-primary">Add an automobile to the inventory!</button>

                </form>
            </div>
        </div>

    );



}


export default NewAutomobile;
