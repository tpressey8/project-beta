import React, {useEffect, useState } from 'react';
import { Link } from "react-router-dom";


function ListService(){
    //set default states
    const [services, setServices] = useState([]);
    const [autos, setAutos] = useState([]);

    //fetch service
    const fetchData = async () => {
        const url = "http://localhost:8080/api/service/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setServices(data.services);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    //get data on automobiles from inventory
    const fetchAutoData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            //set autos state with the data from inventory
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchAutoData();
    }, []);

    function isVip(service){
        //default to false
        let isVipValue = false;
        //for each vin from autos data, check to see if it matches the vin on the service obj
        autos.forEach(auto =>{
            //if it matches, isVipValue will be updated to true
            if(service.vin === auto.vin){
                isVipValue = true;
            }
            // no else since we don't want to toggle it back, just looking for one match
        })
        // return default false assignment if no matches found, otherwise it will be true
        return isVipValue
    }

    async function handleCancelled(id){
        const cancelledUrl = `http://localhost:8080/api/service_cancelled/${id}/`;
        const fetchConfig = {
            method: 'put',
        };
        const cancelledResponse = await fetch(cancelledUrl, fetchConfig)
        if (cancelledResponse.ok) {
            fetchData()

        }
    }
    async function handleFinished(id){
        const finishedUrl = `http://localhost:8080/api/service_finished/${id}/`;
        const fetchConfig = {
            method: 'put',
        };
        const finishedResponse = await fetch(finishedUrl, fetchConfig)
        if (finishedResponse.ok) {
            fetchData()

        }
    }




    ///jsx
    return (
        <table className="table table-striped table-bordered">
        <thead>
            <tr>
                <th > Customer Name </th>
                <th> VIN </th>
                <th> Technician Assigned </th>
                <th > Date </th>
                <th > Time </th>
                <th > Reason </th>
                <th> VIP </th>
                <th > Finished </th>
                <th> Cancelled </th>
            </tr>
        </thead>
        <tbody>
            {services.filter((service)=>service.cancelled===false&&service.finished===false).map((service) => {
            return (
                <tr key = { service.id }>
                    <td> { service.customer_name } </td>
                    <td >{ service.vin }</td>
                    <td >{ service.technician.name}</td>
                    <td >{ new Date(service.date).toLocaleDateString() } </td>
                    <td >{ service.time } </td>
                    <td >{ service.reason }</td>
                    <td> { isVip(service) ? "VIP" : "Not VIP" }</td>
                    <td>
                        <button onClick={() => handleCancelled(service.id)} className="btn btn-lg btn-primary">Service Cancelled: { service.cancelled.toString() }</button>
                    </td>
                    <td>
                        <button onClick={() => handleFinished(service.id)} className="btn btn-lg btn-primary">Service Finished: { service.finished.toString() }</button>
                    </td>
                </tr>
            );
        })}
        </tbody>
        </table>
    )


}

export default ListService;
