import React, {useEffect, useState } from 'react';



function ListVinService(){
    //set default states
    const [services, setServices] = useState([]);
    // const [extraVins, setExtraVins] = useState([])
    const [vins, setVins] = useState([]);
    const [vin, setVin] = useState('');
    const [autos, setAutos] = useState([]);

    const handleVinChange = async (event) =>{
        const value = event.target.value
        setVin(value)
        const specificVinUrl = `http://localhost:8080/api/service/${value}`
        const response = await fetch(specificVinUrl)
        if (response.ok){
            const data = await response.json();
            setServices(data.services)
        }

    }


    //fetch service
    const fetchData = async () => {
        const url = "http://localhost:8080/api/service/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const fetchedSevices = data.services
            setServices(fetchedSevices);
            // console.log(fetchedSevices, "this is fetched services")
            // const fecthedVins = fetchedSevices.map( (serviceObj) => serviceObj.vin )
            // console.log(fecthedVins)

        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    const fetchAutoData = async () => {
        const url = 'http://localhost:8100/api/automobiles/'
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        fetchAutoData();
    }, []);

    function isVip(service){
        let isVipValue = false;
        autos.forEach(auto =>{
            if(service.vin === auto.vin){
                isVipValue = true;
            }
        })
        return isVipValue
    }

    // const vinFilter(services) = services.reduce((accumulator, currentVal) => {
    //     let result = Object.keys(accumulator).includes(currentVal.vin)
    //     if (result===false){
    //         accumulator.push(currentVal)
    //     }
    //     return accumulator
    // }, []);


function vinFiltered(serviceObj){
    let reduced = serviceObj.reduce((accumulator,currentVal) =>{
        let inAccumulator = false;
        accumulator.forEach((element)=>{
            if (element.vin === currentVal.vin){
                inAccumulator = true
            }
        })
        if (inAccumulator === false){
            accumulator.push(currentVal)}
        return accumulator
        }, []);
    return reduced
    }

//should made the dropdown fixed but just makes a copy :'(

    ///jsx
    return (
        <>
         <div className="mb-3">
            <select onChange={handleVinChange}  value={vin} name="vin" id="vin" className='form-select' required>
                <option value="">See history of all service request</option>
                {vinFiltered(services).map(service => {
                    // console.log(service, "---service----")
                return (
                    <option key={service.id} value={service.vin}>{service.vin}</option>
                )
                })}
            </select>
        </div>
        <table className="table table-striped table-bordered">
        <thead>
            <tr>
                <th > Customer Name </th>
                <th> VIN </th>
                <th> Technician Assigned </th>
                <th > Date </th>
                <th > Time </th>
                <th > Reason </th>
                <th> VIP </th>
                <th > Finished </th>
                <th> Cancelled </th>
            </tr>
        </thead>
        <tbody>
            {services.map((service) => {
            return (
                <tr key = { service.id }>
                    <td> { service.customer_name } </td>
                    <td >{ service.vin }</td>
                    <td >{ service.technician.name}</td>
                    <td >{ new Date(service.date).toLocaleDateString() } </td>
                    <td >{ service.time } </td>
                    <td >{ service.reason }</td>
                    <td> { isVip(service) ? "VIP" : "Not VIP" }</td>
                    <td> { service.cancelled.toString() } </td>
                    <td> { service.finished.toString() } </td>
                </tr>
            );
        })}
        </tbody>
        </table>
        </>
    )


}

export default ListVinService;
