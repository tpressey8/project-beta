# CarCar

Team:
* Trey Pressey - Auto Sales Miroservice
* Timi Thompson - Service Microservice

## Design

HOW TO RUN THIS PROJECT:
- Create a data base that this project will use with: docker volume create beta-data
- Create the images specified in the docker-compose file with: docker compose build
- Run the containers specified by the images with: docker compose up

## Service microservice

The service microservice allows the user to create Technicians and Service appointments, view a service history based on an automobile VIN, display lists for all current service requests, and maintain records of finished or cancelled requests. New service requests must be made with an existing technician, and if the vehicle was purchased from the dealership, a VIP status will be added to that service request.

In order to use the service microservice, you will need to create at least one Technician in your database. You can create it one with a name and unique employee number. Then, a service request can be created and assigned to the new technician or any existing technicians. Creating a service will default two properties, cancelled and finished to false. These will be represented in the "Active list" as buttons that can cancel a request or mark it as finished, hiding from the active list. These buttons only exist on the active list, once in history they will become string values and you will not be able to change the properties of cancelled or finished.

To search for the records of service, http://localhost:3000/service/vin will take you to a list of all service record in history. The dropdown will be populated with a list of VINs, selcting on will filter the list by that VIN. The first choice on the list "See history of all service request" will bring you to the unfiltered version of the service records.

Below are the information for api requests through Insomnia/Postman/whatever you use:
https://www.notion.so/045b3857aec848ff873953a69d3d0832?pvs=4#44a06adc06ec43089020206b12c05534
![Alt text](Screen%20Shot%202023-03-10%20at%204.34.27%20PM.png)
![Alt text](Screen%20Shot%202023-03-10%20at%204.34.42%20PM.png)
![Alt text](Screen%20Shot%202023-03-10%20at%204.34.56%20PM.png)
![Alt text](Screen%20Shot%202023-03-10%20at%204.35.06%20PM.png)
![Alt text](Screen%20Shot%202023-03-10%20at%204.35.56%20PM.png)
![Alt text](Screen%20Shot%202023-03-10%20at%204.36.19%20PM.png)

Diagram of Service Microservice and its interaction with the Inventory Microservice
![Alt text](Screen%20Shot%202023-03-10%20at%202.56.42%20PM.png)




## Sales microservice

- POLLER INFO: Since the poller only polls every 60 seconds, the available automobiles for sale won't update with any newly created
  automobileVOs for 60 seconds. I thought about changing how often the poller polls but I am scared to do that for some reason.

- The sales microservice works with the inventory microservice through the poller that
  we have set up (poller.py)
  The data we want from the inventory is the specifics of each automobile which includes the model and manufacturer so we don't need to get those models separatley. The poller hits the API in the inventory APIs that returns a list of all automobiles. Then the poller takes this data and creates new Automobile value objects in the sales microservice. It assigns the VO the href that was on the original Automobile and takes the original automobiles vin to make it more identifiable.
- The models provided in this service are the SalesPerson(fields: Name, Employee ID), Customer(fields: Name, Address, Phone), AutomobileVO
  (fields: import_href, vin, sale_status ), and finally a Sale(fields: customer, sales_person, automobile, sale_price).

HOW TO RUN THIS PROJECT:
- Create a data base that this project will use with: docker volume create beta-data
- Create the images specified in the docker-compose file with: docker compose build
- Run the containers specified by the images with: docker compose up

URLS AND PORTS:
- Port 8090:
    - http://localhost:8090/api/sales/ grants access to a list of all sales
      as well as a dropdown menu that utilizes http://localhost:8090/api/sales/${sales_person_id} to filter the sales made by a specific sales person on
      the front end. Allows for posting a sale with POST method.
    - http://localhost:8090/api/salespeople/ grants access to a list of salespeople with a
      GET request and allows for the creation of a Sales person with a POST
    - http://localhost:8090/api/customers/ grants access to a list of customers with a GET
      request and allowd for the creation of a customer with a POST
    - http://localhost:8090/api/automobiles/sold/3 requires a PUT method and changes the
      sold_status property on a given AutomobileVO object by taking it's id value and then
      changing that property to True so that it won't be available for sale any longer. This happens when a sale form is completed on the front end. The drop down menu that shows the available automobiles filters out automobiles that have their sale_status set to True.
    - http://localhost:8090/api/automobiles/ takes a GET method and returns a list of all
      automobileVO objects that have been created by the polling service

DIAGRAM
    ![Alt text](Screenshot%202023-03-10%20at%2011.42.32%20AM.png)
