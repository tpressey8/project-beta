from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import SalesPerson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone", "id"]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin", "sale_status", "id"]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = ["automobile", "customer", "sales_person", "sale_price", "id"]
    encoders = {
        "customer": CustomerEncoder(),
        "sales_person": SalesPersonEncoder(),
        "automobile": AutomobileVOEncoder(),
    }

#Get and post methods for a sales person
@require_http_methods(["GET", "POST"])
def api_sales_person(request):
    if request.method == "GET":
        sales_people = SalesPerson.objects.all()
        return JsonResponse (
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

#Get and post methods for a customer
@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse (
            {"customers": customers},
            encoder = CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse (
            customer,
            encoder = CustomerEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_sale(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse (
            {"sales": sales},
            encoder = SaleEncoder
        )
    else:
        content = json.loads(request.body)
        try:
                # Get correct sales person based on id
            employee_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=employee_id)
            content["sales_person"] = sales_person

            # Get correct customer based on id
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"] = customer

            # Get correct automobile based on id
            automobile_id = content["automobile"]
            automobile = AutomobileVO.objects.get(id=automobile_id)
            content["automobile"] = automobile

            # Create the sale with all relevant info
            sale = Sale.objects.create(**content)
            print(sale)
            return JsonResponse (
                sale,
                encoder = SaleEncoder,
                safe = False,
            )
        except:
            response = JsonResponse(
                {"message": "Sale didn't go through"}
            )
            response.status_code = 400
            return response



#Get and post methods for the sale object
def api_automobileVO_test(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse (
            {"autos": automobiles},
            encoder = AutomobileVOEncoder
        )

#This url gets hit anytime a sale form is submitted
#through the handlesubmit in the front-end
@require_http_methods(["PUT"])
def api_update_sale_status(request, pk):
    automobile = AutomobileVO.objects.get(id=pk)
    automobile.sale_status = True
    automobile.save()
    return JsonResponse (
        automobile,
        encoder=AutomobileVOEncoder,
        safe=False
    )

# This url gets hit anytime a sales person is selected
# in the sales list
def api_show_sales(request, pk):
    if request.method == "GET":
        sales = Sale.objects.filter(sales_person_id=pk)
        return JsonResponse (
            {"sales": sales},
            encoder=SaleEncoder,
        )
