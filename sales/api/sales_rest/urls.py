from django.urls import path

from .views import (
    api_customer,
    api_sales_person,
    api_automobileVO_test,
    api_update_sale_status,
    api_sale,
    api_show_sales
)

urlpatterns = (
    path("customers/", api_customer, name="api_customer"),
    path("salespeople/", api_sales_person, name="api_sales_person"),
    path("automobiles/", api_automobileVO_test, name="api_automobileVO_test"),
    path("automobiles/sold/<int:pk>", api_update_sale_status, name="api_update_sale_status"),
    path("sales/", api_sale, name="api_sale"),
    path("sales/<int:pk>", api_show_sales, name="api_show_sales"),
)
