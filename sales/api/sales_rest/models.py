from django.db import models
from django.urls import reverse

class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField()


class Customer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone = models.CharField(max_length=50)


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sale_status = models.BooleanField(default=False)


class Sale(models.Model):
    automobile = models.ForeignKey (
        AutomobileVO,
        related_name="sales",
        on_delete=models.CASCADE
    )
    sales_person = models.ForeignKey (
        SalesPerson,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey (
        Customer,
        related_name="sales",
        on_delete=models.CASCADE,
    )
    sale_price = models.CharField(max_length=200)
